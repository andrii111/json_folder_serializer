﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JSerializer.ViewModels;

namespace UnitTest1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            JSONViewModel jViewModel = new JSONViewModel();
            jViewModel.FolderCommand.Execute(new Object());
            //You select any valid folder in automatically opened dialog
            Assert.IsTrue(jViewModel.Label3.Length>0);
        }

        [TestMethod]
        public void TestMethod2()
        {
            JSONViewModel jViewModel = new JSONViewModel();
            jViewModel.FolderCommand.Execute(new Object());
            //You select disk "C:" in automatically opened dialog
            Assert.AreEqual(jViewModel.Label3, @"C:\");
        }
    }
}
